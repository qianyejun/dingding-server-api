package dingding

import "encoding/json"

//获取用户token
func UserAccessToken(UserAccessTokenStruct UserAccessTokenStruct) (UserAccessTokenStructReturn UserAccessTokenStructReturn) {
	str := HttpPost(UserAccessTokenUrl, UserAccessTokenStruct)
	json.Unmarshal([]byte(str), &UserAccessTokenStructReturn)
	return
}
