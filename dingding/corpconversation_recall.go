package dingding

import "encoding/json"

//工作通知撤回
func CorpconversationRecall(access_token string, CorpconversationRecallStruct CorpconversationRecallStruct) (CorpconversationRecallStructReturn CorpconversationRecallStructReturn) {
	str := HttpPost(CorpconversationRecallUrl+"?access_token="+access_token, CorpconversationRecallStruct)
	json.Unmarshal([]byte(str), &CorpconversationRecallStructReturn)
	return
}
