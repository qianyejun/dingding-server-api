package dingding

import "encoding/json"

//创建jsapi_ticket
func JsapiTicket(JsapiTicketStruct JsapiTicketStruct) (JsapiTicketReturn JsapiTicketReturnReturn) {
	str := HttpPost(JsapiTicketUrl, JsapiTicketStruct)
	json.Unmarshal([]byte(str), &JsapiTicketReturn)
	return
}
