package dingding

import "encoding/json"

//激活应用
//参数说明
//suite_access_token:第三方应用的suite_access_token，可调用服务商获取第三方应用授权企业的access_token接口获取。
//Body参数(必填)
func AerviceActivateAuite(suite_access_token string, AerviceActivateAuiteStruct AerviceActivateAuiteStruct) (AerviceActivateAuiteStructReturn AerviceActivateAuiteStructReturn) {
	str := HttpPost(AerviceActivateAuiteUrl+"?suite_access_token="+suite_access_token, AerviceActivateAuiteStruct)
	json.Unmarshal([]byte(str), &AerviceActivateAuiteStructReturn)
	return
}
