package dingding

import "encoding/json"

//获取微应用后台免登的access_token
/*
*参数描述：
*corpid：企业的corpid。（必填）
*corpsecret：可以在开发者后台的基本信息 > 开发信息（旧版）页面获取微应用管理后台SSOSecret。（必填）
 */
func GetSsoGettoken(corpid string, corpsecret string) (GetSsoGettokenReturn GetSsoGetTokenStructReturn) {
	str := HttpGet(GetSsoGettokenUrl + "?corpid=" + corpid + "corpsecret=" + corpsecret)
	json.Unmarshal([]byte(str), &GetSsoGettokenReturn)
	return

}
