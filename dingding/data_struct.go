package dingding

//获取企业内部token返回数据结构体
type GetTokenStructReturn struct {
	AccessToken string `json:"access_token"` //生成的access_token。
	ExpiresIn   int    `json:"expires_in"`   //access_token的过期时间，单位秒。
	Errmsg      string `json:"errmsg"`       //返回码描述。
	Errcode     int    `json:"errcode"`      //返回码
}

//服务商获取第三方应用授权企业的access_token传递结构体
type GetCorpTokenParameterStruct struct {
	AccessKey   string `json:"accessKey"`   //定制应用的CustomKey或者第三方企业应用的SuiteKey（必填）
	Timestamp   string `json:"timestamp"`   //时间戳
	SuiteTicket string `json:"suiteTicket"` //钉钉推送的suiteTicket。
	Signature   string `json:"signature"`   //签名，签名计算方式请参考第三方访问接口的签名计算方法。
	AuthCorpid  string `json:"auth_corpid"` //授权企业的CorpId。
}

//服务商获取第三方应用授权企业的access_token返回数据
type GetCorpTokenParameterStructReturn struct {
	ExpiresIn   string `json:"expires_in"`   //授权企业的access_token超时时间，单位秒。
	AccessToken string `json:"access_token"` //授权企业的access_token。
	Errmsg      string `json:"errmsg"`       //返回码的描述。
	Errcode     string `json:"errcode"`      //返回码。
}

//创建jsapi_ticket参数
type JsapiTicketStruct struct {
	XAcsDingtalkAccessToken string `json:"x-acs-dingtalk-access-token"` //调用服务端API的应用凭证。
}

//创建jsapi_ticket返回
type JsapiTicketReturnReturn struct {
	JsapiTicket string `json:"jsapiTicket"` //返回的jsapi_ticket。
	ExpireIn    string `json:"expireln"`    //jsapi_ticket超时时间。
}

//获取jsapi_ticket返回
type GetJsapiTicketStructReturn struct {
	ExpiresIn string `json:"expires_in"` //jsapi_ticket超时时间。
	Ticket    string `json:"ticket"`     //生成的临时jsapi_ticket。
	Errmsg    string `json:"errmsg"`     //返回码描述
	Errcode   string `json:"errcode"`    //返回码
}

//获取微应用后台免登的access_token
type GetSsoGetTokenStructReturn struct {
	AccessToken string `json:"access_token"` //获取到的凭证。
	Errmsg      string `json:"errmsg"`       //错误信息
	Errcode     string `json:"errcode"`      //错误码
}

//获取登录用户的访问凭证参数
type GetAuthStruct struct {
	RedirectUri     string `json:"redirect_uri"`    //授权通过/拒绝后回调地址。(必填)
	ResponseType    string `json:"response_type"`   //固定值为code。授权通过后返回authCode。(必填)
	ClientId        string `json:"client_id"`       //步骤一中创建的应用详情中获取,企业内部应用：client_id为应用的AppKey。企业内部应用：client_id为应用的AppKey。(必填)
	Scope           string `json:"scope"`           //授权范围，授权页面显示的授权信息以应用注册时配置的为准。(必填)
	State           string `json:"state"`           //跟随authCode原样返回（选填）
	Prompt          string `json:"prompt"`          //值为consent时，会进入授权确认页（必填）
	OrgType         string `json:"org_type"`        //控制输出特定类型的组织列表，org_type=management 表示只输出有管理权限的组织（选填）
	CorpId          string `json:"corpId"`          //用于指定用户需要选择的组织（选填）
	ExclusiveLogin  string `json:"exclusiveLogin"`  //true表示专属帐号登录，展示组织代码输入页。（选填）
	ExclusiveCorpId string `json:"exclusiveCorpId"` //开启了专属帐号功能的组织corpId。（选填）
}

//获取登录用户的token
type UserAccessTokenStruct struct {
	ClientId     string `json:"clientId"`     //应用id。(必填)
	ClientSecret string `json:"clientSecret"` //应用密钥。（选填）
	Code         string `json:"code"`         //OAuth 2.0 临时授权码（选填）
	RefreshToken string `json:"refreshToken"` //OAuth2.0刷新令牌，从返回结果里面获取。（选填）
	GrantType    string `json:"grantType"`    //(选填)
}

//获取登录用户的token返回
type UserAccessTokenStructReturn struct {
	AccessToken  string `json:"accessToken"`  //生成的accessToken
	RefreshToken string `json:"refreshToken"` //生成的refresh_token。可以使用此刷新token，定期的获取用户的accessToken
	ExpireIn     string `json:"expireIn"`     //超时时间，单位秒
	CorpId       string `json:"corpId"`       //所选企业corpId
}

//激活应用参数
type AerviceActivateAuiteStruct struct {
	SuiteKey      string `json:"suite_key"`      //第三方应用的SuiteKey(必填)
	AuthCorpid    string `json:"auth_corpid"`    //授权企业的CorpId。(必填)
	PermanentCode string `json:"permanent_code"` //授权企业的永久授权码。（必填）
}

//激活应用返回
type AerviceActivateAuiteStructReturn struct {
	Errmsg  string `json:"errmsg"`  //返回码描述。
	Errcode string `json:"errcode"` //返回码
}

//工作消息撤回参数
type CorpconversationRecallStruct struct {
	AgentId   string `json:"agent_id"`    //发送消息时使用的微应用的ID。（必填）
	MsgTaskId string `json:"msg_task_id"` //发送消息时钉钉返回的任务ID。（必填）
}

//工作消息撤回返回
type CorpconversationRecallStructReturn struct {
	Errmsg  string `json:"errmsg"`  //返回码描述。
	Errcode int    `json:"errcode"` //返回码。
}

///
//查询用户详情传递报文数据
type GetUserinfoBody struct {
	Userid   string `json:"userid"`   //用户id
	Language string `json:"language"` //通讯录语言
}
