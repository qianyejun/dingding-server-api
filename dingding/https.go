package dingding

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

//get请求
func HttpGet(url string) (str string) {
	reqs, err := http.Get(url)
	fmt.Println(err)
	defer reqs.Body.Close()
	body, _ := ioutil.ReadAll(reqs.Body)
	str = string(body)
	return
}

func HttpPost(url string, body_str interface{}) (str string) {
	json, _ := json.Marshal(&body_str)
	reqs, err := http.Post(url, "application/json", strings.NewReader(string(json)))
	fmt.Println(err)
	defer reqs.Body.Close()
	body, _ := ioutil.ReadAll(reqs.Body)
	str = string(body)
	return
}
