package dingding

const GetTokenUrl string = "https://oapi.dingtalk.com/gettoken"                                             //获取企业内部的access_token
const GetCorpTokenUrl string = "https://oapi.dingtalk.com/service/get_corp_token"                           //服务商获取第三方应用授权企业的access_token
const JsapiTicketUrl string = "https://api.dingtalk.com/v1.0/oauth2/jsapiTickets"                           //创建jsapi_ticket
const GetJsapiTicketUrl string = "https://oapi.dingtalk.com/get_jsapi_ticket"                               //获取jsapi_ticket
const GetSsoGettokenUrl string = "https://oapi.dingtalk.com/sso/gettoken"                                   //获取微应用后台免登的access_token
const AuthUrl string = "https://login.dingtalk.com/oauth2/auth"                                             //获取登录用户的访问凭证
const UserAccessTokenUrl string = "https://api.dingtalk.com/v1.0/oauth2/userAccessToken"                    //获取用户token
const GetUserUrl string = "https://oapi.dingtalk.com/topapi/v2/user/get"                                    //获取用户信息接口
const AerviceActivateAuiteUrl string = "https://oapi.dingtalk.com/service/activate_suite"                   //激活应用
const CorpconversationRecallUrl string = "https://oapi.dingtalk.com/topapi/message/corpconversation/recall" //工作消息撤回
