package dingding

import "encoding/json"

func Auth(GetAuth GetAuthStruct) (GetAuths GetAuthStruct) {
	str := HttpGet(AuthUrl + "?redirect_uri+" + GetAuth.RedirectUri + "response_type=" + GetAuth.ResponseType + "client_id=" + GetAuth.ClientId + "scope=" + GetAuth.Scope + "state=" + GetAuth.State + "prompt=" + GetAuth.Prompt + "org_type=" + GetAuth.OrgType + "corpId=" + GetAuth.CorpId + "exclusiveLogin=" + GetAuth.ExclusiveLogin + "exclusiveCorpId=" + GetAuth.ExclusiveCorpId)
	json.Unmarshal([]byte(str), &GetAuths)
	return
}
