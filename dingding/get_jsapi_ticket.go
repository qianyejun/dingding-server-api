package dingding

import "encoding/json"

//获取jsapi_ticket
/*
*参数描述：
*access_token：调用服务端API的应用凭证。
 */
func GetJsapiTicket(access_token string) (GetJsapiTicketReturn GetJsapiTicketStructReturn) {
	str := HttpGet(GetJsapiTicketUrl + "?access_token=" + access_token)
	json.Unmarshal([]byte(str), &GetJsapiTicketReturn)
	return
}
