//获取企业token
package dingding

import (
	"encoding/json"
)

/*
*参数说明：
*appkey：应用唯一标识key（必填）示例参数：dingeqqpkv3xxxx
*appsecret：应用的密钥。AppKey和AppSecret可在钉钉开发者后台的应用详情页面获取。（必填）示例参数：GT-lsu-taDAsTsxxxx
*返回参数：GettokenReturn结构体
 */
func GetToken(appkey string, appsecret string) (gettokenReturn GetTokenStructReturn) {
	str := HttpGet(GetTokenUrl + "?appkey=" + appkey + "&appsecret=" + appsecret)
	json.Unmarshal([]byte(str), &gettokenReturn)
	return
}
