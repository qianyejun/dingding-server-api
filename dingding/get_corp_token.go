//服务商获取第三方应用授权企业的access_token
package dingding

import (
	"encoding/json"
)

func GetCorpTokenParameter(getCorpTokenParameter GetCorpTokenParameterStruct) (getCorpTokenParameterReturn GetCorpTokenParameterStructReturn) {
	str := HttpPost(GetCorpTokenUrl, getCorpTokenParameter)
	json.Unmarshal([]byte(str), &getCorpTokenParameterReturn)
	return
}
